public class Main {
    public static void main(String[] args) {
        System.out.println("Сейчас Вы увидите прямоугольник:");
        Figures.square();
        System.out.println("Сейчас Вы увидите прямоугольник:");
        Figures.emptySquare();
        System.out.println("Сейчас Вы увидите круг:");
        Figures.circle();
        System.out.println("Сейчас Вы увидите ромб:");
        Figures.rhombus();
        System.out.println("Сейчас Вы увидите первый треугольник:");
        Figures.triangleOne();
        System.out.println("Сейчас Вы увидите второй треугольник:");
        Figures.triangleTwo();
        System.out.println("Сейчас Вы увидите третий треугольник:");
        Figures.triangleThree();
        System.out.println("Сейчас Вы увидите четвертый треугольник:");
        Figures.triangleFour();
        System.out.println("Сейчас Вы увидите пятый треугольник:");
        Figures.triangleFive();
        System.out.println("Сейчас Вы увидите шестой треугольник:");
        Figures.triangleSix();
        System.out.println("Сейчас Вы увидите седьмой треугольник:");
        Figures.triangleSeven();
        System.out.println("Сейчас Вы увидите восьмой треугольник:");
        Figures.triangleEight();
        System.out.println("Сейчас Вы увидите крест:");
        Figures.cross();


    }
}
