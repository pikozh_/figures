public class Figures {
    public static void square() {

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void emptySquare() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == 1 | i == 2 && j == 1 | j == 2) {
                    System.out.print(" ");
                } else
                    System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void circle() {
        int n = 2 * 3 + 1;

        int x, y;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {

                x = i - 3;
                y = j - 3;

                if (x * x + y * y <= 3 * 3 + 1)
                    System.out.print("*");
                else
                    System.out.print(" ");

                System.out.print(" ");
            }

            System.out.println();
        }
    }

    public static void rhombus() {
        for (int i = 1; i < 5; ++i) {

            for (int j = 5; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < 2 * i; ++j)
                System.out.print("*");

            System.out.println();
        }
        for (int i = 5; i >= 1; --i) {

            for (int j = 5; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < i * 2; ++j)
                System.out.print("*");

            System.out.println();
        }
    }


    public static void triangleOne() {
        for (int i = 0; i < 4; i++) {
            for (int a = 0; a < 4; a++) {
                if (a <= i) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }

    public static void triangleTwo() {
        for (int i = 4; i > 0; i--) {
            for (int a = 4; a > 0; a--) {
                if (i >= a) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }

    public static void triangleThree() {
        for (int i = 4; i > 0; i--) {
            for (int a = 1; a <= 4; a++) {
                if (a >= i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void triangleFour() {
        for (int i = 0; i < 4; i++) {
            for (int a = 0; a < 4; a++) {
                if (a >= i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void triangleFive() {
        for (int i = 1; i <= 5; ++i) {

            for (int j = 5; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < 2 * i; ++j)
                System.out.print("*");

            System.out.println();
        }
    }

    public static void triangleSix() {
        for (int i = 5; i >= 1; --i) {

            for (int j = 5; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < 2 * i; ++j)
                System.out.print("*");

            System.out.println();
        }
    }

    public static void triangleSeven() {
        triangleOne();
        for (int i = 3; i > 0; i--) {
            for (int a = 3; a > 0; a--) {
                if (i >= a) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }

    public static void triangleEight() {
        triangleThree();
        for (int i = 0; i < 4; i++) {
            if (i == 0)
                continue;
            for (int a = 0; a < 4; a++) {
                if (a >= i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void cross() {
        for (int i = 0; i < 5; i++) {
            for (int a = 0; a < 5; a++) {
                if (a == i || a + i == 4) {
                    System.out.print("*");
                } else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}


